import numpy as np
import matplotlib.pyplot as plt
from glob import glob
import os
import sys
from PIL import Image
from sklearn.model_selection import train_test_split
from unet_model import unet_model
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.optimizers import SGD
from metrics import iou, iou_thresholded
from pandas import DataFrame

path_to_data = 'TrainingImages'
image_size = (96, 96)

def load_image(fn, img_size):
    im_array = np.array(Image.open(fn).resize(img_size, Image.NEAREST))
    return im_array


def load_data(path_to_raw, path_to_labels, img_size):
    # Load images
    image_files = sorted(glob(path_to_raw + '/*.jpg'))
    images = np.stack([load_image(filename, img_size) for filename in image_files])

    # Load labels
    label_files = sorted(glob(path_to_labels + '/*.png'))
    labels = np.stack([load_image(filename, img_size) for filename in label_files])

    return images, labels


def train_model():

    imgs_np, masks_np = load_data(path_to_data, path_to_data, image_size)

    x = np.asarray(imgs_np, dtype=np.float32)/np.max(imgs_np)
    y = np.asarray(masks_np, dtype=np.float32) / 0xff


    y = y.reshape(*y.shape, 1)
    x = x.reshape(x.shape[0], x.shape[1], x.shape[2], 1)


    x_train, x_val, y_train, y_val = train_test_split(x, y, test_size=0.3, random_state=0)

    #Initialize network

    input_shape = x_train[0].shape

    model = unet_model(
        input_shape,
        num_classes=1,
        filters=64,
        dropout=0.2,
        num_layers=4,
        output_activation='sigmoid'
    )

    #Compile + train
    
    model_filename = 'unet_model.h5'
    callback_checkpoint = ModelCheckpoint(
        model_filename, 
        verbose=1, 
        monitor='val_loss', 
        save_best_only=True,
    )
    early_stopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=10, verbose=1, mode='auto')

    model.compile(
        optimizer=SGD(lr=0.01, momentum=0.99),
        loss='binary_crossentropy',
        metrics=[iou, iou_thresholded]
    )
    
    history = model.fit(
        x_train, y_train,batch_size=32,
        validation_data=(x_val, y_val),
        epochs=50,
        callbacks=[callback_checkpoint, early_stopping],
        shuffle=True
    )
    
    return history.history

if __name__ == '__main__':
    history = train_model()